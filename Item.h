#ifndef DEF_ITEM
#define DEF_ITEM

#include <iostream>
#include <string>

class Item
{
    protected: // permet l'acces pour les methodes style soin
    
    std::string m_nom_arme;
    int m_coefficient;
    int m_degats;


 
    public:
    int get_coefficient();
    int get_m_degats();
    std::string get_nom();



    Item(std::string arme, int coefficient,int degats);
    ~Item();
};
#endif