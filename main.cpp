#include "Personnage.h"
#include "Guerrier.h"
#include "Magicien.h"
#include "Voleur.h"
#include "Squelette.h"
#include "Araignee.h"
#include "Gobelin.h"
#include "Boss.h"
#include "Item.h"

//#include "Rand.h" // permet de faire le lien avec le .h 


#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;


int main()
{


	Personnage Lutin(1,100,5);
	Personnage Demon(2,500,20);

	Guerrier guignol(3,100,10); //test guerrier

	Lutin.change_niveau();
	std::cout << "La vie de Lutin est de :" << Lutin.getM_vie() << std::endl;
	std::cout << "La vie de Demon est de :" << Demon.getM_vie() << std::endl;

	Lutin.attaquer(Demon);
	std::cout << "La vie de Demon est de :" << Demon.getM_vie() << std::endl;

	std::cout << "La position X de Lutin est :" << Lutin.get_X_pos() << std::endl;
	std::cout << "La position Y de Lutin est :" << Lutin.get_Y_pos() << std::endl;
	std::cout << "La le niveau de Lutin est :" << Lutin.get_niveau() << std::endl;
	std::cout << "La xp de Lutin est :" << Lutin.get_experience() << std::endl;

// tests de deplacement //

	Lutin.go_up();
	std::cout << "La position Y de Lutin est :" << Lutin.get_Y_pos() << std::endl;

	Lutin.go_left();
	std::cout << "La position Y de Lutin est :" << Lutin.get_X_pos() << std::endl;


	Lutin.go_right();

	std::cout << "La position X de Lutin est :" << Lutin.get_X_pos() << std::endl;

	Lutin.go_down();

	std::cout << "La position X de Lutin est :" << Lutin.get_Y_pos() << std::endl;

	Lutin.go_right();

	std::cout << "La position X de Lutin est :" << Lutin.get_X_pos() << std::endl;

	Lutin.go_down();

	std::cout << "La position X de Lutin est :" << Lutin.get_Y_pos() << std::endl;

	Lutin.go_left();

	std::cout << "La position X de Lutin est :" << Lutin.get_X_pos() << std::endl;

	Lutin.go_up();

	std::cout << "La position X de Lutin est :" << Lutin.get_Y_pos() << std::endl;

	std::cout << "La vie de guignol avant charge est de :" << guignol.getM_vie() << std::endl;

	//Lutin.se_blesser(8);

	guignol.charge(Lutin,guignol);


	std::cout << "La vie de guignol apres charge est de :" << guignol.getM_vie() << std::endl;
	std::cout << "La vie de Lutin est de :" << Lutin.getM_vie() << std::endl;

	guignol.soin();

	std::cout << "La vie de guignol est de :" << guignol.getM_vie() << std::endl;

	// Test du Magicien //

	Magicien Merlin(3,100,10); //test Magicien


	std::cout << "La vie de Merlin est de :" << Merlin.getM_vie() << std::endl;

	std::cout << "La position X de Merlin est :" << Merlin.get_X_pos() << std::endl;
	std::cout << "La position Y de Merlin est :" << Merlin.get_Y_pos() << std::endl;
	std::cout << "La le niveau de Merlin est :" << Merlin.get_niveau() << std::endl;
	std::cout << "La xp de Merlin est :" << Merlin.get_experience() << std::endl;
	std::cout << "Le mana de Merlin est :" << Merlin.get_Mana() << std::endl;

	Merlin.BouleDeFeu(Demon);


	std::cout << "Le mana de Merlin est :" << Merlin.get_Mana() << std::endl;
	std::cout << "La vie de Demon apres BDF est de :" << Demon.getM_vie() << std::endl;

	Merlin.regen_mana();
	std::cout << "Le mana de Merlin apres regen  est de  :" << Merlin.get_Mana() << std::endl;

	Merlin.regen_mana();
	std::cout << "Le mana de Merlin apres regen  est de  :" << Merlin.get_Mana() << std::endl; // Pas de depassement

	// Test du voleur //

	Voleur aladin(4,100,10); //test voleur


	std::cout << "La vie de aladin est de :" << aladin.getM_vie() << std::endl;

	std::cout << "La position X de aladin est :" << aladin.get_X_pos() << std::endl;
	std::cout << "La position Y de aladin est :" << aladin.get_Y_pos() << std::endl;
	std::cout << "La le niveau de aladin est :" << aladin.get_niveau() << std::endl;
	std::cout << "La xp de aladin est :" << aladin.get_experience() << std::endl;

	aladin.sournoiserie(Demon);

	std::cout << "La vie de Demon apres sournoiserie est de :" << Demon.getM_vie() << std::endl;

	//Test Items //

	Item Massue("Massue_de_fer",8,10);

	std::cout << "Le nom de l arme est :" << Massue.get_nom() << std::endl;
	std::cout << "Le coefficient de l arme est :" << Massue.get_coefficient() << std::endl;


	Squelette Maurice(10,100,10);
	Araignee Lucifer(11,100,10);
	Gobelin Jakob(12,100,10);
	Boss Affreux_Jojo(13,1000,10);


	std::cout << "La vie de Maurice est de :" << Maurice.getM_vie() << std::endl;

	std::cout << "La position X de Maurice est :" << Maurice.get_X_pos() << std::endl;
	std::cout << "La position Y de Maurice est :" << Maurice.get_Y_pos() << std::endl;
	std::cout << "La le niveau de Maurice est :" << Maurice.get_niveau() << std::endl;
	std::cout << "La xp de Maurice est :" << Maurice.get_experience() << std::endl;


	std::cout << "La vie de Lucifer est de :" << Lucifer.getM_vie() << std::endl;

	std::cout << "La position X de Lucifer est :" << Lucifer.get_X_pos() << std::endl;
	std::cout << "La position Y de Lucifer est :" << Lucifer.get_Y_pos() << std::endl;
	std::cout << "La le niveau de Lucifer est :" << Lucifer.get_niveau() << std::endl;
	std::cout << "La xp de Lucifer est :" << Lucifer.get_experience() << std::endl;


	std::cout << "La vie de Lucifer est de :" << Lucifer.getM_vie() << std::endl;

	std::cout << "La position X de Lucifer est :" << Lucifer.get_X_pos() << std::endl;
	std::cout << "La position Y de Lucifer est :" << Lucifer.get_Y_pos() << std::endl;
	std::cout << "La le niveau de Lucifer est :" << Lucifer.get_niveau() << std::endl;
	std::cout << "La xp de Lucifer est :" << Lucifer.get_experience() << std::endl;

	std::cout << "La vie de Jakob est de :" << Jakob.getM_vie() << std::endl;

	std::cout << "La position X de Jakob est :" << Jakob.get_X_pos() << std::endl;
	std::cout << "La position Y de Jakob est :" << Jakob.get_Y_pos() << std::endl;
	std::cout << "La le niveau de Jakob est :" << Jakob.get_niveau() << std::endl;
	std::cout << "La xp de Jakob est :" << Jakob.get_experience() << std::endl;

	std::cout << "La vie de Affreux_Jojo est de :" << Affreux_Jojo.getM_vie() << std::endl;

	std::cout << "La position X de Affreux_Jojo est :" << Affreux_Jojo.get_X_pos() << std::endl;
	std::cout << "La position Y de Affreux_Jojo est :" << Affreux_Jojo.get_Y_pos() << std::endl;
	std::cout << "La le niveau de Affreux_Jojo est :" << Affreux_Jojo.get_niveau() << std::endl;
	std::cout << "La xp de Affreux_Jojo est :" << Affreux_Jojo.get_experience() << std::endl;

	// test des objets inclus dans le personnage //

	std::cout << "Les degats de l'arme de Jojo valent :" << Affreux_Jojo.get_item_puissance() << std::endl;

	// Test du constructeur par defaut Item //
	//Item mains();


	//std::cout << "Le nom de l arme defaut est :" << mains.get_nom() << std::endl;
	//std::cout << "Le coefficient de l arme defaut  est :" << mains.get_coefficient() << std::endl;
}