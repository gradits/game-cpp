#include "Personnage.h"
#include "Item.h"



    Personnage::Personnage(int id)
    {
        m_vie = 100;                              

    m_degatsArme = 10;//NArme.getDegats(); 
    persoId = id;           
    };

    Personnage::Personnage(int id, int vieAtStart, int degatsArmeAtStart)
    {
      m_vie = vieAtStart;
      m_degatsArme = degatsArmeAtStart;
      persoId = id; 
      X_pos = 0;
      Y_pos = 0;
      niveau = 1;
      experience = 0;
      m_arme = new Item("Main nues",0,1);         
    };

    void Personnage::recevoirDegats(int nombreDegats)
    {

     m_vie = m_vie - nombreDegats;      //IL faut rajouter une fonction coup critique
    };

    void Personnage::attaquer(Personnage &cible)
    {
    cible.recevoirDegats(m_degatsArme);         //Attention aux attribut privés
    };


/*    void Personnage::changerArme(Arme &nouvelleArme)
    {
        m_degatsArme = nouvelleArme.getDegats();
        
    };
*/
    bool Personnage::estVivant()                //tout est dans le nom
    {
    if (m_vie > 0)
        return true;
    else             
        return false;
    };

    Personnage::~Personnage()                       //destructeur
    {
        delete m_arme;             
    };
    
    int Personnage::getM_vie()
    {
       return m_vie; 
    };
    int Personnage::getId()
    {
       return persoId; 
    };
    void Personnage::guerir()
    {
         m_vie = m_vie + 40;
         
         if (m_vie > 100)
         {
            m_vie = 100;
         }    
    };

    // void Personnage::se_blesser(int douleur)
    // {
    //      m_vie = m_vie - douleur;
    //      if (m_vie < 0)
    //         m_vie = 0;    
    // };

    void Personnage::increaseDegats(int combien)
    {
         m_degatsArme = m_degatsArme + combien;
    };

    int Personnage::get_X_pos()
    {
       return X_pos; 
    };

    int Personnage::get_Y_pos()
    {
       return Y_pos; 
    };

    int Personnage::get_niveau()
    {
       return niveau; 
    };

    int Personnage::get_experience()
    {
       return experience; 
    };

    int Personnage::get_item_puissance()
    {
       return m_arme->get_m_degats(); 
    };

    void Personnage::change_niveau()
    {
    	

    	if ((experience > 0) && (experience < 110))
    	{
    		niveau = 1;
    	}

    	else if ((experience >= 110) && (experience < 650))
    	{
    		niveau = 2;
    	}

    	else if ((experience >= 650) && (experience < 1500))
    	{
    		niveau = 3;
    	}

    	else if ((experience >= 1500) && (experience < 2800))
    	{
    		niveau = 4;
    	}

    	else if ((experience >= 2800) && (experience < 4800))
    	{
    		niveau = 5;
    	}

    	else if ((experience >= 4800) && (experience < 7300))
    	{
    		niveau = 6;
    	}

    	else if ((experience >= 7300) && (experience < 10500))
    	{
    		niveau = 7;
    	}

    	else if ((experience >= 10500) && (experience < 14500))
    	{
    		niveau = 8;
    	}

    	else if ((experience >= 14500) && (experience < 19200))
    	{
    		niveau = 9;
    	}

    	else 
    	{
    		niveau = 10;
    	}
    };

    void Personnage::go_up()
    {
    	if ((Y_pos >0) &&(Y_pos<=30))
    	{
    		Y_pos = Y_pos-1 ; 

    	}

    	else
    	{
    		Y_pos = Y_pos;
    	}
    };


	void Personnage::go_down()
	{
		if ((Y_pos >=0) &&(Y_pos<30))
    	{
    		Y_pos = Y_pos+1 ; 

    	}

    	else
    	{
    		Y_pos = Y_pos;
    	}
	};

	void Personnage::go_left()
	{
		if ((X_pos >0) &&(X_pos<=30))
    	{
    		X_pos = X_pos-1 ; 

    	}

    	else
    	{
    		X_pos = X_pos;
    	}
	};


	void Personnage::go_right()
	{
		if ((X_pos >=0) &&(X_pos<30))
    	{
    		X_pos = X_pos+1 ; 

    	}

    	else
    	{
    		X_pos = X_pos;
    	}
	};