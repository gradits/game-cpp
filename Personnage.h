#ifndef DEF_PERSONNAGE
#define DEF_PERSONNAGE

#include "Item.h"

class Personnage
{
    protected: // permet l'acces pour les methodes style soin
    int persoId;        
    int m_vie; // voir protected
    int m_degatsArme; // a changer
    int X_pos;
    int Y_pos;
    int niveau;
    int experience;
    Item *m_arme; //pointeur //


 
    public:
    Personnage(int id);
    Personnage(int id, int vieAtStart, int degatsArmeAtStart);
    void recevoirDegats(int nombreDegats);
    void attaquer(Personnage &cible);
    void guerir();
    void increaseDegats(int combien);
    bool estVivant();
    int getM_vie(); 
    int getId();
    int get_X_pos();
    int get_Y_pos();
    int get_niveau();
    int get_experience();
    int get_item_puissance();
    void change_niveau();
    void go_up();
    void go_down();
    void go_left();
    void go_right();
    //void se_blesser(int douleur);


    ~Personnage();
};
#endif